#include "./headers/propagate_intervals.h"
#include "../flowstar-release/Continuous.h"

using namespace std;
using namespace flowstar;

datatype offset_in_constraint_comb = constr_comb_offset;
// So the above data assumes, that this number is
// same for all the networks you have in the setting,
// and also all the networks are single output network
// with the first one giving the output

int main()
{
	Variables stateVars;

	/*
	 * Declaration of the state variables.
	 * The first one should always be the local time variable which can be
	 * viewed as a preserved variable. It is only used internally by the library.
	 */
	stateVars.declareVar("t");
	stateVars.declareVar("x1");
	stateVars.declareVar("x2");
  stateVars.declareVar("x3");
  stateVars.declareVar("x4");
	stateVars.declareVar("x5");
	stateVars.declareVar("x6");
  stateVars.declareVar("x7");
  stateVars.declareVar("x8");
	stateVars.declareVar("x9");
	stateVars.declareVar("x10");
	stateVars.declareVar("x11");
	stateVars.declareVar("x12");
	stateVars.declareVar("u1");
  stateVars.declareVar("u2");
	stateVars.declareVar("u3");
  stateVars.declareVar("u4");

	int domainDim = 7;

	Expression_AST deriv_x1( "x4" , stateVars);
	Expression_AST deriv_x2( "x5" , stateVars);
	Expression_AST deriv_x3( "x6" , stateVars);
	Expression_AST deriv_x4( "x8 * u1 / 0.033" , stateVars);
	Expression_AST deriv_x5( "-x7 * u1 / 0.033" , stateVars);
	Expression_AST deriv_x6( "(-0.033 * 9.81 + u1 ) / 0.033 " , stateVars);
	Expression_AST deriv_x7( "x10" , stateVars);
	Expression_AST deriv_x8( "x11" , stateVars);
	Expression_AST deriv_x9( "x12" , stateVars);
	Expression_AST deriv_x10( "0.0000279 * (1.0 * ( u2 - x7 ) - x10 ) / 0.00001395 " , stateVars);
	Expression_AST deriv_x11( "0.000028 * (1.0 * ( u3 - x8 ) - x11 ) / 0.00001436" , stateVars);
	Expression_AST deriv_x12( "0.0000435 * (1.0 * ( u4 - x9 ) - x12 ) / 0.00002173" , stateVars);

  Expression_AST deriv_u1("0", stateVars);
  Expression_AST deriv_u2("0", stateVars);
	Expression_AST deriv_u3("0", stateVars);
	Expression_AST deriv_u4("0", stateVars);



	ODE plant(stateVars);
	plant.assignDerivative("x1", deriv_x1);
	plant.assignDerivative("x2", deriv_x2);
  plant.assignDerivative("x3", deriv_x3);
	plant.assignDerivative("x4", deriv_x4);
	plant.assignDerivative("x5", deriv_x5);
	plant.assignDerivative("x6", deriv_x6);
	plant.assignDerivative("x7", deriv_x7);
	plant.assignDerivative("x8", deriv_x8);
	plant.assignDerivative("x9", deriv_x9);
	plant.assignDerivative("x10", deriv_x10);
	plant.assignDerivative("x11", deriv_x11);
	plant.assignDerivative("x12", deriv_x12);

	plant.assignDerivative("u1", deriv_u1);
  plant.assignDerivative("u2", deriv_u2);
	plant.assignDerivative("u3", deriv_u3);
  plant.assignDerivative("u4", deriv_u4);


	/*
	 * Specify the parameters for reachability computation.
	 */
	Continuous_Reachability_Setting crs;

	// step size
	crs.setFixedStepsize(0.01);

	// Taylor model order
	crs.setFixedOrder(100);

	// precision
	crs.setPrecision(100);

	// cutoff threshold
	Interval cutoff(-1e-10,1e-10);
	crs.setCutoff(cutoff);

	/*
	 * A remainder estimation is a vector of intervals such that
	 * the i-th component is the estimation for the i-th state variable.
	 */
	Interval E(-0.01,0.01);
	std::vector<Interval> estimation;
	estimation.push_back(E);	// estimation for the 1st variable
	estimation.push_back(E);	// estimation for the 2nd variable
	estimation.push_back(E);	// estimation for the 3rd variable
  estimation.push_back(E);	// estimation for the 4th variable
  estimation.push_back(E);	// estimation for the 5th variable
  estimation.push_back(E);	// estimation for the 6th variable
  estimation.push_back(E);	// estimation for the 7th variable
	estimation.push_back(E);	// estimation for the 8th variable
	estimation.push_back(E);	// estimation for the 9th variable
	estimation.push_back(E);	// estimation for the 10th variable
	estimation.push_back(E);	// estimation for the 11th variable
	estimation.push_back(E);	// estimation for the 12th variable
	estimation.push_back(E);	// estimation for the 13th variable
	estimation.push_back(E);	// estimation for the 14th variable
	estimation.push_back(E);	// estimation for the 15th variable
	estimation.push_back(E);	// estimation for the 16th variable
	estimation.push_back(E);	// estimation for the 17th variable

	crs.setRemainderEstimation(estimation);

	// call this function whenever a parameter is set or changed
	crs.prepareForReachability();





	// Simple range propagation
	char controller_file[] = "../systems_with_networks/Ex_Drone/NN_sherlock/controller_1.nt" ;
	network_handler system_network(controller_file);

  vector< vector< vector< datatype > > > buff_weights;
  vector< vector < datatype > > buff_biases;

  system_network.return_network_information(buff_weights, buff_biases);
  system_network.cast_to_single_output_network(buff_weights, buff_biases, 1);
  network_handler control_output_1(buff_weights, buff_biases);

  system_network.return_network_information(buff_weights, buff_biases);
  system_network.cast_to_single_output_network(buff_weights, buff_biases, 2);
  network_handler control_output_2(buff_weights, buff_biases);

	system_network.return_network_information(buff_weights, buff_biases);
  system_network.cast_to_single_output_network(buff_weights, buff_biases, 3);
  network_handler control_output_3(buff_weights, buff_biases);

	system_network.return_network_information(buff_weights, buff_biases);
  system_network.cast_to_single_output_network(buff_weights, buff_biases, 4);
  network_handler control_output_4(buff_weights, buff_biases);



	/*
	 * Initial set can be a box which is represented by a vector of intervals.
	 * The i-th component denotes the initial set of the i-th state variable.
	 */
	// Interval init_x1(-0.0225,-0.0224), init_x2(0.3089,0.3090), init_x3(0.2086,0.2087), init_x4(0.1407,0.1408),
	// init_x5(0.6676,0.6677), init_x6(0.3038,0.3039), init_x7(0.0138,0.0139), init_x8(-0.0110,-0.0109),
	// init_x9(0.0093,0.0094), init_x10(-0.0366,-0.0365), init_x11(-0.0002,-0.0001), init_x12(-0.0002,-0.0001),
	// init_u1, init_u2, init_u3, init_u4, intZero;

	double rad = 0.05;
	Interval init_x1(-0.0225,-0.0225 + rad), init_x2(0.3089,0.3089 + rad), init_x3(0.2086,0.2086 + rad), init_x4(0.1407,0.1407 + rad ),
	init_x5(0.6676,0.6676 + rad), init_x6(0.3038,0.3038 + rad), init_x7(0.0138,0.0138 + rad), init_x8(-0.0110,-0.0110 + rad),
	init_x9(0.0093,0.0093 + rad), init_x10(-0.0366,-0.0366 + rad), init_x11(-0.0002,-0.0002 + rad), init_x12(-0.0002,-0.0002 + rad),
	init_u1, init_u2, init_u3, init_u4, intZero;


	std::vector<Interval> X0;
	X0.push_back(init_x1);
	X0.push_back(init_x2);
  X0.push_back(init_x3);
	X0.push_back(init_x4);
	X0.push_back(init_x5);
	X0.push_back(init_x6);
  X0.push_back(init_x7);
	X0.push_back(init_x8);
	X0.push_back(init_x9);
	X0.push_back(init_x10);
  X0.push_back(init_x11);
	X0.push_back(init_x12);

	X0.push_back(init_u1);
  X0.push_back(init_u2);
	X0.push_back(init_u3);
  X0.push_back(init_u4);


	// translate the initial set to a flowpipe
	Flowpipe initial_set(X0, intZero);

	// the flowpipe that keeps the overapproximation at the end of a time horizon
	Flowpipe fp_last;

	// the symbolic remainder
	Symbolic_Remainder symb_rem(initial_set);

	std::list<Flowpipe> result;
	std::list<Flowpipe> flowpipes_end;

	flowpipes_end.push_back(initial_set);
	int linear_piece_count = 0;

	for(int k=0; k<50; ++k)
	{

		std::vector<Interval> NN_input;
		initial_set.intEvalNormal(NN_input, crs.step_end_exp_table, crs.cutoff_threshold);

		vector< vector< datatype > > input_interval(12, vector< datatype >(2,0));

		// Printing the current set
		int a = 0;
		while(a < 12)
		{
			input_interval[a][0] = NN_input[a].inf();
			input_interval[a][1] = NN_input[a].sup();
			cout << "[ " << input_interval[a][0] << " , " << input_interval[a][1] <<  " ] , " ;
			a++;
		}
		cout << endl;

		// printf("[%lf, %lf], \t [%lf, %lf], \t [%lf, %lf], \t [%lf, %lf]\n", input_interval[0][0], input_interval[0][1],
		// 		input_interval[1][0], input_interval[1][1], input_interval[2][0], input_interval[2][1], input_interval[3][0], input_interval[3][1]);

		vector< vector< datatype > > input_constraints;
		create_constraint_from_interval(input_constraints, input_interval);


    // /////////  Control input 1 /////////

		vector< vector< unsigned int > > monomial_terms_1;
		vector< datatype > coefficients_1;
		datatype offset = -100.0;
		datatype scaling = 1.0;
		unsigned int degree = 2;

		generate_polynomial_for_NN(control_output_1, degree, input_constraints, offset, scaling, monomial_terms_1, coefficients_1);
		int i,j,l;

		vector<my_monomial_t> polynomial;
		polynomial = create_polynomial_from_monomials_and_coeffs(monomial_terms_1, coefficients_1);
    // i = 0;
    // for(auto coeff : coefficients)
    // {
    //   cout << coeff << " * [ " << monomial_terms[i][0] <<monomial_terms[i][1] << monomial_terms[i][2] << monomial_terms[i][3] << " ] " ;
    //   i++;
    // }
    // cout << endl;

		vector< vector< vector< datatype > > > weights;
		vector< vector< datatype > > biases;
		control_output_1.return_network_information(weights, biases);

		vector< vector< vector< vector< datatype > > > > region_descriptions_1;
		vector< vector < vector< datatype > > > linear_mapping_1;
    //
    //
		vector< PolynomialApproximator > decomposed_pwls_1;
		vector< double > lower_bounds_1;
		vector< double > upper_bounds_1;

		double tolerance = 1e-5;
		create_PWL_approximation(polynomial, input_interval, tolerance, region_descriptions_1, linear_mapping_1,
			 decomposed_pwls_1, lower_bounds_1, upper_bounds_1, linear_piece_count);

		vector< datatype > difference;
		control_output_1.return_interval_difference_wrt_PWL(input_interval, difference, 1, region_descriptions_1,
			 linear_mapping_1, offset, scaling, decomposed_pwls_1, lower_bounds_1, upper_bounds_1);

		datatype optima = compute_max_abs_in_a_vector(difference);
		cout << " Difference is : " << optima << endl;


		Polynomial poly_u_1;

		for(int i=0; i < monomial_terms_1.size(); ++i)
		{
			monomial_terms_1[i].insert(monomial_terms_1[i].begin(), 0);
			monomial_terms_1[i].push_back(0);

			Monomial monomial(coefficients_1[i], *((vector<int> *) &monomial_terms_1[i]));

			poly_u_1.monomials.push_back(monomial);
		}

		poly_u_1.reorder();

		TaylorModel tm_u_1;

		vector<Interval> polyRange_initial_set_1;
		initial_set.tmvPre.polyRangeNormal(polyRange_initial_set_1, crs.step_end_exp_table);

/*
		for(int i=0; i<initial_set.tmvPre.tms.size(); ++i)
		{
			initial_set.tmvPre.tms[i].dump_interval(stdout, stateVars.varNames);printf("\n");
		}
		printf("\n");
*/

		poly_u_1.insert_normal(tm_u_1, initial_set.tmvPre, polyRange_initial_set_1, crs.step_end_exp_table, domainDim, crs.cutoff_threshold);

		Interval intError(-optima, optima);
		tm_u_1.remainder += intError;

		initial_set.tmvPre.tms[12] = tm_u_1;

    /////// Control input 2 ///////

    vector< vector< unsigned int > > monomial_terms_2;
		vector< datatype > coefficients_2;

		generate_polynomial_for_NN(control_output_2, degree, input_constraints, offset, scaling, monomial_terms_2, coefficients_2);

		vector<my_monomial_t> polynomial_2;
		polynomial_2 = create_polynomial_from_monomials_and_coeffs(monomial_terms_2, coefficients_2);

    weights.clear();
    biases.clear();
		control_output_2.return_network_information(weights, biases);

		vector< vector< vector< vector< datatype > > > > region_descriptions_2;
		vector< vector < vector< datatype > > > linear_mapping_2;


		vector< PolynomialApproximator > decomposed_pwls_2;
		vector< double > lower_bounds_2;
		vector< double > upper_bounds_2;

		tolerance = 1e-5;
		create_PWL_approximation(polynomial_2, input_interval, tolerance, region_descriptions_2, linear_mapping_2,
			 decomposed_pwls_2, lower_bounds_2, upper_bounds_2, linear_piece_count);

		difference.clear();
		control_output_2.return_interval_difference_wrt_PWL(input_interval, difference, 1, region_descriptions_2,
			 linear_mapping_2, offset, scaling, decomposed_pwls_2, lower_bounds_2, upper_bounds_2);

		optima = compute_max_abs_in_a_vector(difference);
		cout << " Difference is : " << optima << endl;

		Polynomial poly_u_2;

		for(int i=0; i<monomial_terms_2.size(); ++i)
		{
			monomial_terms_2[i].insert(monomial_terms_2[i].begin(), 0);
			monomial_terms_2[i].push_back(0);

			Monomial monomial(coefficients_2[i], *((vector<int> *) &monomial_terms_2[i]));

			poly_u_2.monomials.push_back(monomial);
		}

		poly_u_2.reorder();

		TaylorModel tm_u_2;

		vector<Interval> polyRange_initial_set_2;
		initial_set.tmvPre.polyRangeNormal(polyRange_initial_set_2, crs.step_end_exp_table);


		poly_u_2.insert_normal(tm_u_2, initial_set.tmvPre, polyRange_initial_set_2, crs.step_end_exp_table, domainDim, crs.cutoff_threshold);

		Interval intError_2(-optima, optima);
		tm_u_2.remainder += intError_2;

		initial_set.tmvPre.tms[13] = tm_u_2;

    ////////////////////////////////////////

		/////// Control input 3 ///////

		vector< vector< unsigned int > > monomial_terms_3;
		vector< datatype > coefficients_3;

		generate_polynomial_for_NN(control_output_3, degree, input_constraints, offset, scaling, monomial_terms_3, coefficients_3);

		vector<my_monomial_t> polynomial_3;
		polynomial_3 = create_polynomial_from_monomials_and_coeffs(monomial_terms_3, coefficients_3);

		weights.clear();
		biases.clear();
		control_output_3.return_network_information(weights, biases);

		vector< vector< vector< vector< datatype > > > > region_descriptions_3;
		vector< vector < vector< datatype > > > linear_mapping_3;


		vector< PolynomialApproximator > decomposed_pwls_3;
		vector< double > lower_bounds_3;
		vector< double > upper_bounds_3;

		tolerance = 1e-5;
		create_PWL_approximation(polynomial_3, input_interval, tolerance, region_descriptions_3, linear_mapping_3,
			 decomposed_pwls_3, lower_bounds_3, upper_bounds_3, linear_piece_count);

		difference.clear();
		control_output_3.return_interval_difference_wrt_PWL(input_interval, difference, 1, region_descriptions_3,
			 linear_mapping_3, offset, scaling, decomposed_pwls_3, lower_bounds_3, upper_bounds_3);

		optima = compute_max_abs_in_a_vector(difference);
		cout << " Difference is : " << optima << endl;

		Polynomial poly_u_3;

		for(int i=0; i < monomial_terms_3.size(); ++i)
		{
			monomial_terms_3[i].insert(monomial_terms_3[i].begin(), 0);
			monomial_terms_3[i].push_back(0);

			Monomial monomial(coefficients_3[i], *((vector<int> *) &monomial_terms_3[i]));

			poly_u_3.monomials.push_back(monomial);
		}

		poly_u_3.reorder();

		TaylorModel tm_u_3;

		vector<Interval> polyRange_initial_set_3;
		initial_set.tmvPre.polyRangeNormal(polyRange_initial_set_3, crs.step_end_exp_table);

		poly_u_3.insert_normal(tm_u_3, initial_set.tmvPre, polyRange_initial_set_3, crs.step_end_exp_table, domainDim, crs.cutoff_threshold);

		Interval intError_3(-optima, optima);
		tm_u_3.remainder += intError_3;

		initial_set.tmvPre.tms[14] = tm_u_3;

		////////////////////////////////////////

		/////// Control input 4 ///////

    vector< vector< unsigned int > > monomial_terms_4;
		vector< datatype > coefficients_4;

		generate_polynomial_for_NN(control_output_4, degree, input_constraints, offset, scaling, monomial_terms_4, coefficients_4);

		vector<my_monomial_t> polynomial_4;
		polynomial_4 = create_polynomial_from_monomials_and_coeffs(monomial_terms_4, coefficients_4);

    weights.clear();
    biases.clear();
		control_output_4.return_network_information(weights, biases);

		vector< vector< vector< vector< datatype > > > > region_descriptions_4;
		vector< vector < vector< datatype > > > linear_mapping_4;


		vector< PolynomialApproximator > decomposed_pwls_4;
		vector< double > lower_bounds_4;
		vector< double > upper_bounds_4;

		tolerance = 1e-5;
		create_PWL_approximation(polynomial_4, input_interval, tolerance, region_descriptions_4, linear_mapping_4,
			 decomposed_pwls_4, lower_bounds_4, upper_bounds_4, linear_piece_count);

		difference.clear();
		control_output_4.return_interval_difference_wrt_PWL(input_interval, difference, 1, region_descriptions_4,
			 linear_mapping_4, offset, scaling, decomposed_pwls_4, lower_bounds_4, upper_bounds_4);

		optima = compute_max_abs_in_a_vector(difference);
		cout << " Difference is : " << optima << endl;

		Polynomial poly_u_4;

		for(int i=0; i<monomial_terms_4.size(); ++i)
		{
			monomial_terms_4[i].insert(monomial_terms_4[i].begin(), 0);
			monomial_terms_4[i].push_back(0);

			Monomial monomial(coefficients_4[i], *((vector<int> *) &monomial_terms_4[i]));

			poly_u_4.monomials.push_back(monomial);
		}

		poly_u_4.reorder();

		TaylorModel tm_u_4;

		vector<Interval> polyRange_initial_set_4;
		initial_set.tmvPre.polyRangeNormal(polyRange_initial_set_4, crs.step_end_exp_table);

		poly_u_4.insert_normal(tm_u_4, initial_set.tmvPre, polyRange_initial_set_4, crs.step_end_exp_table, domainDim, crs.cutoff_threshold);

		Interval intError_4(-optima, optima);
		tm_u_4.remainder += intError_4;

		initial_set.tmvPre.tms[15] = tm_u_4;



    ////////////////////////////////////////

		printf("Step %d\n", k);




		bool res = plant.reach_symbolic_remainder(result, fp_last, symb_rem, crs, initial_set, 10, 200);
		// bool res = plant.reach_interval_remainder(result, fp_last, crs, initial_set, 10);

		if(res)
		{
			initial_set = fp_last;

//			flowpipes_end.push_back(fp_last);
		}
		else
		{
			printf("Terminated due to too large overestimation.\n");
			break;
		}
	}


	// plot the flowpipes in the x-y plane
	FILE *fp = fopen("Ex_Drone_Model_LR.m", "w");
	plot_2D_interval_MATLAB(fp, "x1", "x2", stateVars, result);
	fclose(fp);



  return 0;
}
