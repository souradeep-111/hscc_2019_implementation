#include "./headers/compute_flowpipes.h"

using namespace std;
using namespace flowstar;

datatype offset_in_constraint_comb = constr_comb_offset;


int main()
{
	Variables stateVars;

	/*
	 * Declaration of the state variables.
	 * The first one should always be the local time variable which can be
	 * viewed as a preserved variable. It is only used internally by the library.
	 */


	double time_step = 0.1;
	int no_of_flowpipes = 2;
	int no_of_steps = 50;

	int polynomial_degree_for_controller = 1;

	stateVars.declareVar("t");
	stateVars.declareVar("x_1");
	stateVars.declareVar("x_2");
	stateVars.declareVar("x_3");
	stateVars.declareVar("x_4");
	stateVars.declareVar("x_5");
	stateVars.declareVar("x_6");
	stateVars.declareVar("u");

	int domainDim = 8;

	Expression_AST deriv_x_1("x_3", stateVars);
	Expression_AST deriv_x_2("x_4", stateVars);
	Expression_AST deriv_x_3("x_5", stateVars);
	Expression_AST deriv_x_4("x_6", stateVars);
	Expression_AST deriv_x_5("-2*x_5 - 4 - 0.0001*x_3^2", stateVars);
	Expression_AST deriv_x_6("-2*x_6 + 2*u - 0.0001*x_4^2", stateVars);
	Expression_AST deriv_u("0", stateVars);

	ODE plant(stateVars);
	plant.assignDerivative("x_1", deriv_x_1);
	plant.assignDerivative("x_2", deriv_x_2);
	plant.assignDerivative("x_3", deriv_x_3);
	plant.assignDerivative("x_4", deriv_x_4);
	plant.assignDerivative("x_5", deriv_x_5);
	plant.assignDerivative("x_6", deriv_x_6);
	plant.assignDerivative("u", deriv_u);


	/*
	 * Specify the parameters for reachability computation.
	 */
	Continuous_Reachability_Setting crs;

	// step size
	crs.setFixedStepsize(time_step/((double) no_of_flowpipes));

	// Taylor model order
	crs.setFixedOrder(4);

	// precision
	crs.setPrecision(100);

	// cutoff threshold
	Interval cutoff(-1e-10,1e-10);
	crs.setCutoff(cutoff);

	/*
	 * A remainder estimation is a vector of intervals such that
	 * the i-th component is the estimation for the i-th state variable.
	 */
	Interval E(-0.01,0.01);
	std::vector<Interval> estimation;
	estimation.push_back(E);	// estimation for the 1st variable
	estimation.push_back(E);	// estimation for the 2nd variable
	estimation.push_back(E);	// estimation for the 3rd variable
	estimation.push_back(E);	// estimation for the 4th variable
	estimation.push_back(E);	// estimation for the 5th variable
	estimation.push_back(E);	// estimation for the 6th variable
	estimation.push_back(E);	// estimation for the 7th variable


	crs.setRemainderEstimation(estimation);

	// call this function whenever a parameter is set or changed
	crs.prepareForReachability();





	// Simple range propagation
	char controller_file[] = "../systems_with_networks/ARCH_2019/ACC/modified_controller_2.nt" ;
	network_handler system_network(controller_file);

	vector< network_handler > all_controllers;
	all_controllers.push_back(system_network);

	vector< double > network_offsets;
	network_offsets.push_back(-10.0);

	vector< double > network_scaling_factors;
	network_scaling_factors.push_back(1.0);

	/*
	 * Initial set can be a box which is represented by a vector of intervals.
	 * The i-th component denotes the initial set of the i-th state variable.
	 */


	Interval init_x_1(90,91), init_x_2(10,11), init_x_3(30.0,30.2), init_x_4(30.0,30.2),
	init_x_5(0.0,0.01), init_x_6(0.0,0.01), init_u, intZero;

	std::vector<Interval> X0;
	X0.push_back(init_x_1);
	X0.push_back(init_x_2);
	X0.push_back(init_x_3);
	X0.push_back(init_x_4);
	X0.push_back(init_x_5);
	X0.push_back(init_x_6);
	X0.push_back(init_u);


	pair<int, int> plot_dimensions = make_pair(-1, 2);
	string filename_to_save = "./../../../Plots/Ex_arch.m";


	// translate the initial set to a flowpipe
	Flowpipe initial_set(X0, intZero);

	// the flowpipe that keeps the overapproximation at the end of a time horizon
	Flowpipe fp_last;

	// the symbolic remainder
	Symbolic_Remainder symb_rem(initial_set);

	std::list<Flowpipe> result;
	std::list<Flowpipe> flowpipes_end;

	flowpipes_end.push_back(initial_set);


	std:: chrono::duration< double > time_span;
	std :: chrono :: steady_clock::time_point start_time;
	std :: chrono :: steady_clock::time_point end_time;


	map< string, double > timing_information;

	std :: chrono :: steady_clock::time_point t1 = std :: chrono :: steady_clock::now();


	compute_flowpipes_for_n_steps(
		X0, no_of_steps, no_of_flowpipes,
		polynomial_degree_for_controller, plant, crs,
		all_controllers, network_offsets, network_scaling_factors,
		result, filename_to_save, plot_dimensions, timing_information
	);



	std :: chrono :: steady_clock::time_point t2 = std::chrono::steady_clock::now();
	time_span = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1);
	cout << "Total execution time =  " << time_span.count() << " seconds." << endl;

	cout << "\% of time spent in regression = " << timing_information["time_in_regression"] * 100.0 << " \% " << endl;
	cout << "\% of  time spent in PWL construction = " << timing_information["time_in_pwl_construction"] * 100.0<< " \% "  << endl;
	cout << "\% of  time spent in Sherlock = " << timing_information["time_in_sherlock"] * 100.0 << " \% " <<  endl;
	cout << "\% of  time spent in Flowstar = " << timing_information["time_in_flowstar"] * 100.0 << " \% " << endl;
	cout << "Max Linear Pieces = " << timing_information["max_linear_pieces"] << endl;
	cout << "Max Error encountered = " << timing_information["max_error_encountered"] << endl;

	// plot the flowpipes in the x-y plane
	FILE *fp = fopen("./../../../Plots/Ex_arch.m", "w");
 	plot_2D_interval_MATLAB(fp, "x_3", "x_4", stateVars, result);
	// plot_2D_box_gnuplot(fp, "gnuplot", result, "x", "y", stateVars, cutoff);
	// fclose(fp);


	// max_difference and max_linear_pieces are global variables defined inside compute_flowpipes.h
	save_final_results_to_file(true, 1 ,4,0.01,2, timing_information["max_error_encountered"], time_span.count(),timing_information["time_in_regression"] * 100.0
	,timing_information["time_in_pwl_construction"] * 100.0, timing_information["time_in_sherlock"] * 100.0,
	timing_information["time_in_flowstar"] * 100.0, timing_information["max_linear_pieces"]);

	return 0;





}
