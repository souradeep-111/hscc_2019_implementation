#include "./headers/compute_flowpipes.h"

using namespace std;
using namespace flowstar;

datatype offset_in_constraint_comb = constr_comb_offset;

int main()
{
	Variables stateVars;

	/*
	 * Declaration of the state variables.
	 * The first one should always be the local time variable which can be
	 * viewed as a preserved variable. It is only used internally by the library.
	 */

	double time_step = 1;
	int no_of_flowpipes = 100;
	int no_of_steps = 10;

	int polynomial_degree_for_controller = 1;

	stateVars.declareVar("t");
	stateVars.declareVar("pn");
	stateVars.declareVar("pe");
	stateVars.declareVar("h");
	stateVars.declareVar("u");
	stateVars.declareVar("v");
	stateVars.declareVar("w");
	stateVars.declareVar("q0");
	stateVars.declareVar("q1");
	stateVars.declareVar("q2");
	stateVars.declareVar("q3");
	stateVars.declareVar("p");
	stateVars.declareVar("q");
	stateVars.declareVar("r");
	stateVars.declareVar("pI");
	stateVars.declareVar("qI");
	stateVars.declareVar("rI");
	stateVars.declareVar("hI");
	stateVars.declareVar("T");
	stateVars.declareVar("input");

	int domainDim = 20;
	int rangeDim = 19;


	Expression_AST deriv_pn("2*u*(q0^2 + q1^2 - 0.5) - 2*v*(q0*q3 - q1*q2) + 2*w*(q0*q2 + q1*q3)", stateVars);
	Expression_AST deriv_pe("2*v*(q0^2 + q2^2 - 0.5) + 2*u*(q0*q3 + q1*q2) - 2*w*(q0*q1 - q2*q3)", stateVars);
	Expression_AST deriv_h("2*w*(q0^2 + q3^2 - 0.5) - 2*u*(q0*q2 - q1*q3) + 2*v*(q0*q1 + q2*q3)", stateVars);
	Expression_AST deriv_u("r*v - q*w - 11.62*(q0*q2 - q1*q3)", stateVars);
	Expression_AST deriv_v("p*w - r*u + 11.62*(q0*q1 + q2*q3)", stateVars);
	Expression_AST deriv_w("q*u - p*v + 11.62*(q0^2 + q3^2 - 0.5) + input + [-0.001, 0.001]", stateVars);
	Expression_AST deriv_q0("-0.5*q1*p - 0.5*q2*q - 0.5*q3*r", stateVars);
	Expression_AST deriv_q1("0.5*q0*p - 0.5*q3*q + 0.5*q2*r", stateVars);
	Expression_AST deriv_q2("0.5*q3*p + 0.5*q0*q - 0.5*q1*r", stateVars);
	Expression_AST deriv_q3("0.5*q1*q - 0.5*q2*p + 0.5*q0*r", stateVars);
	Expression_AST deriv_p("(-40.00063258437631*pI - 2.8283979829540325*p) - 1.133407423682400*q*r", stateVars);
	Expression_AST deriv_q("(-39.99980452524146*qI - 2.8283752541008109*q) + 1.132078179613602*p*r", stateVars);
	Expression_AST deriv_r("(-39.99978909742505*rI - 2.8284134223281210*r) - 0.004695219977601*p*q", stateVars);
	Expression_AST deriv_pI("p", stateVars);
	Expression_AST deriv_qI("q", stateVars);
	Expression_AST deriv_rI("r", stateVars);
	Expression_AST deriv_hI("h", stateVars);
	Expression_AST deriv_T("1", stateVars);
	Expression_AST deriv_input("0", stateVars);


	ODE plant(stateVars);
	plant.assignDerivative("pn", deriv_pn);
	plant.assignDerivative("pe", deriv_pe);
	plant.assignDerivative("h", deriv_h);
	plant.assignDerivative("u", deriv_u);
	plant.assignDerivative("v", deriv_v);
	plant.assignDerivative("w", deriv_w);
	plant.assignDerivative("q0", deriv_q0);
	plant.assignDerivative("q1", deriv_q1);
	plant.assignDerivative("q2", deriv_q2);
	plant.assignDerivative("q3", deriv_q3);
	plant.assignDerivative("p", deriv_p);
	plant.assignDerivative("q", deriv_q);
	plant.assignDerivative("r", deriv_r);
	plant.assignDerivative("pI", deriv_pI);
	plant.assignDerivative("qI", deriv_qI);
	plant.assignDerivative("rI", deriv_rI);
	plant.assignDerivative("hI", deriv_hI);
	plant.assignDerivative("T", deriv_T);
	plant.assignDerivative("input", deriv_input);



	/*
	 * Specify the parameters for reachability computation.
	 */
	Continuous_Reachability_Setting crs;

	// step size
	crs.setFixedStepsize(time_step / ( (double) no_of_flowpipes ));

	// Taylor model order
	crs.setFixedOrder(5);

	// precision
	crs.setPrecision(100);

	// cutoff threshold
	Interval cutoff(-1e-6,1e-6);
	crs.setCutoff(cutoff);

	/*
	 * A remainder estimation is a vector of intervals such that
	 * the i-th component is the estimation for the i-th state variable.
	 */
	Interval E(-0.01,0.01);
	std::vector<Interval> estimation;

	for(int i=0; i<rangeDim; ++i)
	{
		estimation.push_back(E);
	}
	crs.setRemainderEstimation(estimation);

	// call this function whenever a parameter is set or changed
	crs.prepareForReachability();





	// Simple range propagation
	// char controller_file[] = "../systems_with_networks/Ex_Quadrotor/neural_network_controller" ;
	char controller_file[] = "../systems_with_networks/Ex_Quadrotor/trial_controller_3" ;
	network_handler system_network(controller_file);

	vector< network_handler > all_controllers;
	all_controllers.push_back(system_network);

	vector< double > network_offsets;
	network_offsets.push_back(-100.0);

	vector< double > network_scaling_factors;
	network_scaling_factors.push_back(1.0);


	/*
	 * Initial set can be a box which is represented by a vector of intervals.
	 * The i-th component denotes the initial set of the i-th state variable.
	 */
	 Interval init_pn(-1,-0.99), init_pe(-1,-0.99), init_h(9,9.01), init_u(-1,-0.99),
 			init_v(-1,-0.99), init_w(-1,-0.99), init_q0, init_q1,
 			init_q2, init_q3(1,1), init_p(-1,-0.99), init_q(-1,-0.99),
 			init_r(-1,-0.99), init_pI, init_qI, init_rI,
 			init_hI, init_T,
 			init_input, intZero;

	std::vector<Interval> X0;
	X0.push_back(init_pn);
	X0.push_back(init_pe);
	X0.push_back(init_h);
	X0.push_back(init_u);
	X0.push_back(init_v);
	X0.push_back(init_w);
	X0.push_back(init_q0);
	X0.push_back(init_q1);
	X0.push_back(init_q2);
	X0.push_back(init_q3);
	X0.push_back(init_p);
	X0.push_back(init_q);
	X0.push_back(init_r);
	X0.push_back(init_pI);
	X0.push_back(init_qI);
	X0.push_back(init_rI);
	X0.push_back(init_hI);
	X0.push_back(init_T);
	X0.push_back(init_input);

	pair<int, int> plot_dimensions = make_pair(-1, 2);
	string filename_to_save = "./../../../Plots/Ex_Quadrotor_.m";


	// translate the initial set to a flowpipe
	Flowpipe initial_set(X0, intZero);

	// the flowpipe that keeps the overapproximation at the end of a time horizon
	Flowpipe fp_last;

	// the symbolic remainder
	Symbolic_Remainder symb_rem(initial_set);

	std::list<Flowpipe> result;
	std::list<Flowpipe> flowpipes_end;

	flowpipes_end.push_back(initial_set);


		std:: chrono::duration< double > time_span;
		std :: chrono :: steady_clock::time_point start_time;
		std :: chrono :: steady_clock::time_point end_time;


		map< string, double > timing_information;

		std :: chrono :: steady_clock::time_point t1 = std :: chrono :: steady_clock::now();


		compute_flowpipes_for_n_steps(
			X0, no_of_steps, no_of_flowpipes,
			polynomial_degree_for_controller, plant, crs,
			all_controllers, network_offsets, network_scaling_factors,
			result,  filename_to_save, plot_dimensions, timing_information
		);



		std :: chrono :: steady_clock::time_point t2 = std::chrono::steady_clock::now();
		time_span = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1);
		cout << "Total execution time =  " << time_span.count() << " seconds." << endl;

		cout << "\% of time spent in regression = " << timing_information["time_in_regression"] * 100.0 << " \% " << endl;
		cout << "\% of  time spent in PWL construction = " << timing_information["time_in_pwl_construction"] * 100.0<< " \% "  << endl;
		cout << "\% of  time spent in Sherlock = " << timing_information["time_in_sherlock"] * 100.0 << " \% " <<  endl;
		cout << "\% of  time spent in Flowstar = " << timing_information["time_in_flowstar"] * 100.0 << " \% " << endl;
		cout << "Max Linear Pieces = " << timing_information["max_linear_pieces"] << endl;
		cout << "Max Error encountered = " << timing_information["max_error_encountered"] << endl;


	// plot the flowpipes in the x-y plane
	FILE *fp = fopen("./../../../Plots/Ex_Quadrotor.m", "w");
	plot_2D_interval_MATLAB(fp, "T", "h", stateVars, result);
	fclose(fp);


	save_final_results_to_file(false, 11 ,5,0.01,1, timing_information["max_error_encountered"], time_span.count(),timing_information["time_in_regression"] * 100.0
	,timing_information["time_in_pwl_construction"] * 100.0, timing_information["time_in_sherlock"] * 100.0,
	timing_information["time_in_flowstar"] * 100.0, timing_information["max_linear_pieces"]);



	return 0;
}
