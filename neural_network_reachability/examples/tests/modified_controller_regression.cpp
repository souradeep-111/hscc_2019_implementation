#include "./headers/propagate_intervals.h"
#include "../flowstar-release/Continuous.h"

using namespace std;
using namespace flowstar;

datatype offset_in_constraint_comb = constr_comb_offset;
// So the above data assumes, that this number is
// same for all the networks you have in the setting,
// and also all the networks are single output network
// with the first one giving the output

int main()
{
	Variables stateVars;

	/*
	 * Declaration of the state variables.
	 * The first one should always be the local time variable which can be
	 * viewed as a preserved variable. It is only used internally by the library.
	 */
	stateVars.declareVar("t");
	stateVars.declareVar("x");
	stateVars.declareVar("y");
	stateVars.declareVar("u");

	int domainDim = 4;


	/*
	 * Define the first continuous dynamics.
	 * It is the ODE definition of a Van der Pol oscillator.
	 */
	Expression_AST deriv_x("y", stateVars);
	Expression_AST deriv_y("u*y^2 - x", stateVars);
	Expression_AST deriv_u("0", stateVars);

	ODE plant(stateVars);
	plant.assignDerivative("x", deriv_x);
	plant.assignDerivative("y", deriv_y);
	plant.assignDerivative("u", deriv_u);


	/*
	 * Specify the parameters for reachability computation.
	 */
	Continuous_Reachability_Setting crs;

	// step size
	crs.setFixedStepsize(0.02);

	// Taylor model order
	crs.setFixedOrder(6);

	// precision
	crs.setPrecision(100);

	// cutoff threshold
	Interval cutoff(-1e-10,1e-10);
	crs.setCutoff(cutoff);

	/*
	 * A remainder estimation is a vector of intervals such that
	 * the i-th component is the estimation for the i-th state variable.
	 */
	Interval E(-0.01,0.01);
	std::vector<Interval> estimation;
	estimation.push_back(E);	// estimation for the 1st variable
	estimation.push_back(E);	// estimation for the 2nd variable
	estimation.push_back(E);	// estimation for the 3rd variable
	crs.setRemainderEstimation(estimation);

	// call this function whenever a parameter is set or changed
	crs.prepareForReachability();





	// Simple range propagation
	char controller_file[] = "./network_files/modified_controller" ;
	network_handler system_network(controller_file);



	/*
	 * Initial set can be a box which is represented by a vector of intervals.
	 * The i-th component denotes the initial set of the i-th state variable.
	 */
	Interval init_x(0.5,0.55), init_y(0.5,0.55), init_u, intZero;
	std::vector<Interval> X0;
	X0.push_back(init_x);
	X0.push_back(init_y);
	X0.push_back(init_u);




	// translate the initial set to a flowpipe
	Flowpipe initial_set(X0, intZero);

	// the flowpipe that keeps the overapproximation at the end of a time horizon
	Flowpipe fp_last;

	// the symbolic remainder
	Symbolic_Remainder symb_rem(initial_set);

	std::list<Flowpipe> result;


	for(int k=0; k<200; ++k)
	{
		bool res = plant.reach_symbolic_remainder(result, fp_last, symb_rem, crs, initial_set, 10, 200);
//		bool res = plant.reach_interval_remainder(result, fp_last, crs, initial_set, 10);

		if(res)
		{
			initial_set = fp_last;

			std::vector<Interval> NN_input;
			fp_last.intEvalNormal(NN_input, crs.step_end_exp_table, crs.cutoff_threshold);

			vector< vector< datatype > > input_interval(2, vector< datatype >(2,0));
			input_interval[0][0] = NN_input[0].inf();
			input_interval[0][1] = NN_input[0].sup();

			input_interval[1][0] = NN_input[1].inf();
			input_interval[1][1] = NN_input[1].sup();

			printf("[%lf, %lf], \t [%lf, %lf]\n", input_interval[0][0], input_interval[0][1],
					input_interval[1][0], input_interval[1][1]);

			vector< vector< datatype > > input_constraints;
			create_constraint_from_interval(input_constraints, input_interval);


			vector< vector< unsigned int > > monomial_terms;
			vector< datatype > coefficients;
			datatype offset = -4;
			datatype scaling = 1.0; //(0.1 if the scaling was by 10)
			generate_polynomial_for_NN(system_network, 1, input_constraints, offset, scaling, monomial_terms, coefficients);


			vector< vector< vector< datatype > > > weights;
			vector< vector< datatype > > biases;
			system_network.return_network_information(weights, biases);

			vector< vector< vector< datatype > > > region_descriptions;
			region_descriptions.push_back(input_interval);
			vector< vector< datatype > > linear_mapping;
			vector< datatype > linear_map;
			linear_map.push_back(coefficients[1]);
			linear_map.push_back(coefficients[2]);
			linear_map.push_back(coefficients[0]);
			linear_mapping.push_back(linear_map);

			datatype optima;

			optimize_diff_NN_and_PWL(input_constraints, weights, biases, region_descriptions, linear_mapping, optima, offset, scaling );
			cout << "Max difference = " << optima << endl;



			Polynomial poly_u;

			for(int i=0; i<monomial_terms.size(); ++i)
			{
				monomial_terms[i].insert(monomial_terms[i].begin(), 0);
				monomial_terms[i].push_back(0);

				Monomial monomial(coefficients[i], *((vector<int> *) &monomial_terms[i]));

				poly_u.monomials.push_back(monomial);
			}

			poly_u.reorder();

			TaylorModel tm_u;

			vector<Interval> polyRange_initial_set;
			initial_set.tmvPre.polyRangeNormal(polyRange_initial_set, crs.step_end_exp_table);

/*
			for(int i=0; i<initial_set.tmvPre.tms.size(); ++i)
			{
				initial_set.tmvPre.tms[i].dump_interval(stdout, stateVars.varNames);printf("\n");
			}
			printf("\n");
*/

			poly_u.insert_normal(tm_u, initial_set.tmvPre, polyRange_initial_set, crs.step_end_exp_table, domainDim, crs.cutoff_threshold);

			Interval intError(-optima, optima);
			tm_u.remainder += intError;

			initial_set.tmvPre.tms[2] = tm_u;

//tm_u.dump_interval(stdout, stateVars.varNames);printf("\n");

			printf("Step %d\n", k);







/*
			vector< datatype > output_range(2,0);
		 	system_network.return_interval_output(input_constraints, output_range, 1);

		 	cout << "output_range = [" << output_range[0] << " , " << output_range[1] << " ]" << endl;

		 	Interval u_range(output_range[0], output_range[1]);
		 	u_range -= 4;			// -4

		 	Interval M;
		 	u_range.remove_midpoint(M);

		 	TaylorModel tm_u_const(M, domainDim), tm_u(u_range.sup(), domainDim);
		 	tm_u.mul_assign(4, 1);

		 	tm_u.add_assign(tm_u_const);

		 	initial_set.tmvPre.tms[2] = tm_u;
*/
		}
		else
		{
			printf("Terminated due to too large overestimation.\n");
			break;
		}
	}

//printf("1\n");


	// plot the flowpipes in the x-y plane
	FILE *fp = fopen("modified_controller_regression.m", "w");
	plot_2D_interval_MATLAB(fp, "x", "y", stateVars, result);
	fclose(fp);

//printf("2\n");




  return 0;
}
