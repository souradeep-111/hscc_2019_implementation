#include "./headers/propagate_intervals.h"
#include "../flowstar-release/Continuous.h"

using namespace std;
using namespace flowstar;

datatype offset_in_constraint_comb = constr_comb_offset;
// So the above data assumes, that this number is
// same for all the networks you have in the setting,
// and also all the networks are single output network
// with the first one giving the output

int main()
{
	Variables stateVars;

	/*
	 * Declaration of the state variables.
	 * The first one should always be the local time variable which can be
	 * viewed as a preserved variable. It is only used internally by the library.
	 */
	stateVars.declareVar("t");
	stateVars.declareVar("pn");
	stateVars.declareVar("pe");
	stateVars.declareVar("h");
	stateVars.declareVar("u");
	stateVars.declareVar("v");
	stateVars.declareVar("w");
	stateVars.declareVar("q0");
	stateVars.declareVar("q1");
	stateVars.declareVar("q2");
	stateVars.declareVar("q3");
	stateVars.declareVar("p");
	stateVars.declareVar("q");
	stateVars.declareVar("r");
	stateVars.declareVar("pI");
	stateVars.declareVar("qI");
	stateVars.declareVar("rI");
	stateVars.declareVar("hI");
	stateVars.declareVar("T");
	stateVars.declareVar("input");

	int domainDim = 20;
	int rangeDim = 19;


	Expression_AST deriv_pn("2*u*(q0^2 + q1^2 - 0.5) - 2*v*(q0*q3 - q1*q2) + 2*w*(q0*q2 + q1*q3)", stateVars);
	Expression_AST deriv_pe("2*v*(q0^2 + q2^2 - 0.5) + 2*u*(q0*q3 + q1*q2) - 2*w*(q0*q1 - q2*q3)", stateVars);
	Expression_AST deriv_h("2*w*(q0^2 + q3^2 - 0.5) - 2*u*(q0*q2 - q1*q3) + 2*v*(q0*q1 + q2*q3)", stateVars);
	Expression_AST deriv_u("r*v - q*w - 11.62*(q0*q2 - q1*q3)", stateVars);
	Expression_AST deriv_v("p*w - r*u + 11.62*(q0*q1 + q2*q3)", stateVars);
	Expression_AST deriv_w("q*u - p*v + 11.62*(q0^2 + q3^2 - 0.5) + input + [-0.001, 0.001]", stateVars);
	Expression_AST deriv_q0("-0.5*q1*p - 0.5*q2*q - 0.5*q3*r", stateVars);
	Expression_AST deriv_q1("0.5*q0*p - 0.5*q3*q + 0.5*q2*r", stateVars);
	Expression_AST deriv_q2("0.5*q3*p + 0.5*q0*q - 0.5*q1*r", stateVars);
	Expression_AST deriv_q3("0.5*q1*q - 0.5*q2*p + 0.5*q0*r", stateVars);
	Expression_AST deriv_p("(-40.00063258437631*pI - 2.8283979829540325*p) - 1.133407423682400*q*r", stateVars);
	Expression_AST deriv_q("(-39.99980452524146*qI - 2.8283752541008109*q) + 1.132078179613602*p*r", stateVars);
	Expression_AST deriv_r("(-39.99978909742505*rI - 2.8284134223281210*r) - 0.004695219977601*p*q", stateVars);
	Expression_AST deriv_pI("p", stateVars);
	Expression_AST deriv_qI("q", stateVars);
	Expression_AST deriv_rI("r", stateVars);
	Expression_AST deriv_hI("h", stateVars);
	Expression_AST deriv_T("1", stateVars);
	Expression_AST deriv_input("0", stateVars);


	ODE plant(stateVars);
	plant.assignDerivative("pn", deriv_pn);
	plant.assignDerivative("pe", deriv_pe);
	plant.assignDerivative("h", deriv_h);
	plant.assignDerivative("u", deriv_u);
	plant.assignDerivative("v", deriv_v);
	plant.assignDerivative("w", deriv_w);
	plant.assignDerivative("q0", deriv_q0);
	plant.assignDerivative("q1", deriv_q1);
	plant.assignDerivative("q2", deriv_q2);
	plant.assignDerivative("q3", deriv_q3);
	plant.assignDerivative("p", deriv_p);
	plant.assignDerivative("q", deriv_q);
	plant.assignDerivative("r", deriv_r);
	plant.assignDerivative("pI", deriv_pI);
	plant.assignDerivative("qI", deriv_qI);
	plant.assignDerivative("rI", deriv_rI);
	plant.assignDerivative("hI", deriv_hI);
	plant.assignDerivative("T", deriv_T);
	plant.assignDerivative("input", deriv_input);



	/*
	 * Specify the parameters for reachability computation.
	 */
	Continuous_Reachability_Setting crs;

	// step size
	crs.setFixedStepsize(0.01);

	// Taylor model order
	crs.setFixedOrder(5);

	// precision
	crs.setPrecision(100);

	// cutoff threshold
	Interval cutoff(-1e-6,1e-6);
	crs.setCutoff(cutoff);

	/*
	 * A remainder estimation is a vector of intervals such that
	 * the i-th component is the estimation for the i-th state variable.
	 */
	Interval E(-0.01,0.01);
	std::vector<Interval> estimation;

	for(int i=0; i<rangeDim; ++i)
	{
		estimation.push_back(E);
	}
	crs.setRemainderEstimation(estimation);

	// call this function whenever a parameter is set or changed
	crs.prepareForReachability();





	// Simple range propagation
	// char controller_file[] = "../systems_with_networks/Ex_Quadrotor/neural_network_controller" ;
	char controller_file[] = "../systems_with_networks/Ex_Quadrotor/trial_controller_3" ;

	network_handler system_network(controller_file);



	/*
	 * Initial set can be a box which is represented by a vector of intervals.
	 * The i-th component denotes the initial set of the i-th state variable.
	 */
	 Interval init_pn(-1,-0.99), init_pe(-1,-0.99), init_h(9,9.01), init_u(-1,-0.99),
 			init_v(-1,-0.99), init_w(-1,-0.99), init_q0, init_q1,
 			init_q2, init_q3(1,1), init_p(-1,-0.99), init_q(-1,-0.99),
 			init_r(-1,-0.99), init_pI, init_qI, init_rI,
 			init_hI, init_T,
 			init_input, intZero;

	std::vector<Interval> X0;
	X0.push_back(init_pn);
	X0.push_back(init_pe);
	X0.push_back(init_h);
	X0.push_back(init_u);
	X0.push_back(init_v);
	X0.push_back(init_w);
	X0.push_back(init_q0);
	X0.push_back(init_q1);
	X0.push_back(init_q2);
	X0.push_back(init_q3);
	X0.push_back(init_p);
	X0.push_back(init_q);
	X0.push_back(init_r);
	X0.push_back(init_pI);
	X0.push_back(init_qI);
	X0.push_back(init_rI);
	X0.push_back(init_hI);
	X0.push_back(init_T);
	X0.push_back(init_input);




	// translate the initial set to a flowpipe
	Flowpipe initial_set(X0, intZero);

	// the flowpipe that keeps the overapproximation at the end of a time horizon
	Flowpipe fp_last;

	// the symbolic remainder
	Symbolic_Remainder symb_rem(initial_set);

	std::list<Flowpipe> result;
	std::list<Flowpipe> flowpipes_end;

	flowpipes_end.push_back(initial_set);
	datatype max_difference = 0.0;
	std :: chrono :: steady_clock::time_point t1 = std :: chrono :: steady_clock::now();


	std:: chrono::duration< double > time_span;
	std :: chrono :: steady_clock::time_point start_time;
	std :: chrono :: steady_clock::time_point end_time;

	// std:: chrono::duration< double > total_time_spent_in_regression  = 0;
	double total_time_spent_in_regression  = 0;
	double total_time_spent_in_PWL_construction  = 0;
	double total_time_spent_in_Sherlock  = 0;
	double total_time_spent_in_Flowstar  = 0;
	int linear_piece_count = 0;
	int max_linear_pieces = 0;

	int NN_input_size = rangeDim - 1;

	for(int k=0; k<10; ++k)
	{
		std::vector<Interval> NN_input;
		initial_set.intEvalNormal(NN_input, crs.step_end_exp_table, crs.cutoff_threshold);

		vector< vector< datatype > > input_interval(NN_input_size, vector< datatype >(2,0));

		for(int i=0; i<NN_input_size; ++i)
		{
			input_interval[i][0] = NN_input[i].inf();
			input_interval[i][1] = NN_input[i].sup();
		}


//		printf("[%lf, %lf], \t [%lf, %lf]\t [%lf, %lf]\t [%lf, %lf]\n", input_interval[0][0], input_interval[0][1],
//				input_interval[1][0], input_interval[1][1], input_interval[2][0], input_interval[2][1], input_interval[3][0], input_interval[3][1]);


		vector< vector< datatype > > input_constraints;
		create_constraint_from_interval(input_constraints, input_interval);


		vector< vector< unsigned int > > monomial_terms;
		vector< datatype > coefficients;
		datatype offset = -100;
		datatype scaling = 1;

		unsigned int degree = 1;

		start_time = std :: chrono :: steady_clock::now();

		generate_polynomial_for_NN(system_network, degree, input_constraints, offset, scaling, monomial_terms, coefficients);
		int i,j,l;

		vector<my_monomial_t> polynomial;
		polynomial = create_polynomial_from_monomials_and_coeffs(monomial_terms, coefficients);

		end_time = std :: chrono :: steady_clock::now();

		time_span = std::chrono::duration_cast<std::chrono::duration<double>>(end_time - start_time);
		total_time_spent_in_regression += time_span.count();


		vector< vector< vector< datatype > > > weights;
		vector< vector< datatype > > biases;
		system_network.return_network_information(weights, biases);

		vector< vector< vector< vector< datatype > > > > region_descriptions;
		vector< vector < vector< datatype > > > linear_mapping;
    //
    //
		vector< PolynomialApproximator > decomposed_pwls;
		vector< double > lower_bounds;
		vector< double > upper_bounds;

		double tolerance = 1e-5;

		start_time = std :: chrono :: steady_clock::now();

		create_PWL_approximation(polynomial, input_interval, tolerance, region_descriptions, linear_mapping,
			 decomposed_pwls, lower_bounds, upper_bounds,linear_piece_count);
			 if(linear_piece_count > max_linear_pieces)
		 	{
		 		max_linear_pieces = linear_piece_count;
		 	}
	 end_time = std :: chrono :: steady_clock::now();
	time_span = std::chrono::duration_cast<std::chrono::duration<double>>(end_time - start_time);
	total_time_spent_in_PWL_construction += time_span.count();

	start_time = std :: chrono :: steady_clock::now();

		vector< datatype > difference;
		system_network.return_interval_difference_wrt_PWL(input_interval, difference, 1, region_descriptions,
			 linear_mapping, offset, scaling, decomposed_pwls, lower_bounds, upper_bounds);

			 end_time = std :: chrono :: steady_clock::now();
	   	time_span = std::chrono::duration_cast<std::chrono::duration<double>>(end_time - start_time);
	 		total_time_spent_in_Sherlock += time_span.count();


		datatype optima = compute_max_abs_in_a_vector(difference);
		if(optima > max_difference)
		{
			max_difference = optima;
		}
		// cout << "Difference is : " << optima << endl;




/*

		generate_polynomial_for_NN(system_network, 1, input_constraints, offset, scaling, monomial_terms, coefficients);


		vector< vector< vector< datatype > > > weights;
		vector< vector< datatype > > biases;
		system_network.return_network_information(weights, biases);

		vector< vector< vector< datatype > > > region_descriptions;
		region_descriptions.push_back(input_interval);
		vector< vector< datatype > > linear_mapping;
		vector< datatype > linear_map;

		for(int i=1; i<=NN_input_size; ++i)
		{
			linear_map.push_back(coefficients[i]);
		}

		linear_map.push_back(coefficients[0]);
		linear_mapping.push_back(linear_map);


		vector< datatype > difference;
		system_network.return_interval_difference_wrt_PWL(input_interval, difference, 1, region_descriptions, linear_mapping, offset, scaling);

		datatype optima = compute_max_abs_in_a_vector(difference);
		cout << "Difference is : " << optima << endl;
*/

		start_time = std :: chrono :: steady_clock::now();

		Polynomial poly_u;

		for(int i=0; i<monomial_terms.size(); ++i)
		{
			monomial_terms[i].insert(monomial_terms[i].begin(), 0);
			monomial_terms[i].push_back(0);

			Monomial monomial(coefficients[i], *((vector<int> *) &monomial_terms[i]));

			poly_u.monomials.push_back(monomial);
		}

		poly_u.reorder();

		TaylorModel tm_u;

		vector<Interval> polyRange_initial_set;
		initial_set.tmvPre.polyRangeNormal(polyRange_initial_set, crs.step_end_exp_table);



		poly_u.insert_normal(tm_u, initial_set.tmvPre, polyRange_initial_set, crs.step_end_exp_table, domainDim, crs.cutoff_threshold);

		Interval intError(-optima, optima);
		tm_u.remainder += intError;

		initial_set.tmvPre.tms[18] = tm_u;

		printf("Step %d\n", k);




		bool res = plant.reach_symbolic_remainder(result, fp_last, symb_rem, crs, initial_set, 100, 200);
//		bool res = plant.reach_interval_remainder(result, fp_last, crs, initial_set, 10);

		if(res)
		{
			initial_set = fp_last;

//			flowpipes_end.push_back(fp_last);
		}
		else
		{
			printf("Terminated due to too large overestimation.\n");
			break;
		}

		end_time = std :: chrono :: steady_clock::now();
		time_span = std::chrono::duration_cast<std::chrono::duration<double>>(end_time - start_time);
		total_time_spent_in_Flowstar += time_span.count();

	}

	cout << "Max error encountered = " << max_difference << endl;
	cout << "Max Linear pieces = " << max_linear_pieces << endl;

	std :: chrono :: steady_clock::time_point t2 = std::chrono::steady_clock::now();
	time_span = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1);
	cout << "Total execution time =  " << time_span.count() << " seconds." << endl;

	cout << "\% of time spent in regression = " << (total_time_spent_in_regression/time_span.count()) * 100.0 << " \% " << endl;
	cout << "\% of  time spent in PWL construction = " << (total_time_spent_in_PWL_construction/time_span.count()) * 100.0<< " \% "  << endl;
	cout << "\% of  time spent in Sherlock = " << (total_time_spent_in_Sherlock/time_span.count()) * 100.0 << " \% " <<  endl;
	cout << "\% of  time spent in Flowstar = " << (total_time_spent_in_Flowstar/time_span.count()) * 100.0 << " \% " << endl;



	// plot the flowpipes in the x-y plane
	FILE *fp = fopen("./../../../Plots/Ex_Quadrotor_PR.m", "w");
	plot_2D_interval_MATLAB(fp, "T", "h", stateVars, result);
	fclose(fp);


	save_final_results_to_file(false, 11 ,5,0.01,1,max_difference,time_span.count(),(total_time_spent_in_regression/time_span.count()) * 100.0
	,(total_time_spent_in_PWL_construction/time_span.count()) * 100.0,(total_time_spent_in_Sherlock/time_span.count()) * 100.0,
	(total_time_spent_in_Flowstar/time_span.count()) * 100.0,max_linear_pieces);



	return 0;
}
