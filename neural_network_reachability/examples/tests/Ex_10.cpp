#include "./headers/compute_flowpipes.h"

using namespace std;
using namespace flowstar;

datatype offset_in_constraint_comb = constr_comb_offset;
// So the above data assumes, that this number is
// same for all the networks you have in the setting,
// and also all the networks are single output network
// with the first one giving the output

int main()
{
	Variables stateVars;

	/*
	 * Declaration of the state variables.
	 * The first one should always be the local time variable which can be
	 * viewed as a preserved variable. It is only used internally by the library.
	 */

	double time_step = 0.2;
	int no_of_flowpipes = 20;
	int no_of_steps = 50;

	int polynomial_degree_for_controller = 2;

	stateVars.declareVar("t");
	stateVars.declareVar("x1");
	stateVars.declareVar("x2");
  stateVars.declareVar("x3");
  stateVars.declareVar("x4");
	stateVars.declareVar("u1");
  stateVars.declareVar("u2");

	int domainDim = 7;

	// Expression_AST deriv_x1( " x4*cos(x3 + 0.4545*(sin(u2)/cos(u2)) - ((0.4545*(sin(u2)/cos(u2)))^3)/3 + ((0.4545*(sin(u2)/cos(u2)))^5)/5) " , stateVars);
  // Expression_AST deriv_x2( " x4*sin(x3 + 0.4545*(sin(u2)/cos(u2)) - ((0.4545*(sin(u2)/cos(u2)))^3)/3 + ((0.4545*(sin(u2)/cos(u2)))^5)/5) " , stateVars);
  // Expression_AST deriv_x3( " (x4 * sin( 0.4545*(sin(u2)/cos(u2)) - ((0.4545*(sin(u2)/cos(u2)))^3)/3 + ((0.4545*(sin(u2)/cos(u2)))^5)/5) )/1.5 ", stateVars);

  Expression_AST deriv_x1( " x4 * cos(x3) " , stateVars);
  Expression_AST deriv_x2( " x4 * sin(x3) " , stateVars);
  Expression_AST deriv_x3( "u2 ", stateVars);
  Expression_AST deriv_x4("u1 + [-0.0001,0.0001] ", stateVars);
  Expression_AST deriv_u1("0", stateVars);
  Expression_AST deriv_u2("0", stateVars);



	ODE plant(stateVars);
	plant.assignDerivative("x1", deriv_x1);
	plant.assignDerivative("x2", deriv_x2);
  plant.assignDerivative("x3", deriv_x3);
	plant.assignDerivative("x4", deriv_x4);
	plant.assignDerivative("u1", deriv_u1);
  plant.assignDerivative("u2", deriv_u2);


	/*
	 * Specify the parameters for reachability computation.
	 */
	Continuous_Reachability_Setting crs;

	// step size
	crs.setFixedStepsize(time_step / ((double) no_of_flowpipes) );

	// Taylor model order
	crs.setFixedOrder(30);

	// precision
	crs.setPrecision(100);

	// cutoff threshold
	Interval cutoff(-1e-10,1e-10);
	crs.setCutoff(cutoff);

	/*
	 * A remainder estimation is a vector of intervals such that
	 * the i-th component is the estimation for the i-th state variable.
	 */
	Interval E(-0.01,0.01);
	std::vector<Interval> estimation;
	estimation.push_back(E);	// estimation for the 1st variable
	estimation.push_back(E);	// estimation for the 2nd variable
	estimation.push_back(E);	// estimation for the 3rd variable
  estimation.push_back(E);	// estimation for the 4th variable
  estimation.push_back(E);	// estimation for the 5th variable
  estimation.push_back(E);	// estimation for the 6th variable
  estimation.push_back(E);	// estimation for the 7th variable
	crs.setRemainderEstimation(estimation);

	// call this function whenever a parameter is set or changed
	crs.prepareForReachability();





	// Simple range propagation
	char controller_file[] = "../systems_with_networks/Ex_car_model/NN_sherlock/neural_network_controller_2" ;
  // char controller_file[] = "../systems_with_networks/Ex_car_model/NN_sherlock/neural_network_controller_5" ;

	network_handler system_network(controller_file);

  vector< vector< vector< datatype > > > buff_weights;
  vector< vector < datatype > > buff_biases;
  system_network.return_network_information(buff_weights, buff_biases);
  system_network.cast_to_single_output_network(buff_weights, buff_biases, 1);
  network_handler control_output_1(buff_weights, buff_biases);

  system_network.return_network_information(buff_weights, buff_biases);
  system_network.cast_to_single_output_network(buff_weights, buff_biases, 2);
  network_handler control_output_2(buff_weights, buff_biases);

	vector< network_handler > all_controllers;
	all_controllers.push_back(control_output_1);
	all_controllers.push_back(control_output_2);

	vector< double > network_offsets;
	network_offsets.push_back(-20.0);
	network_offsets.push_back(-20.0);

	vector< double > network_scaling_factors;
	network_scaling_factors.push_back(1.0);
	network_scaling_factors.push_back(1.0);

	/*
	 * Initial set can be a box which is represented by a vector of intervals.
	 * The i-th component denotes the initial set of the i-th state variable.
	 */
	 Interval init_x1(9.5,9.55), init_x2(-4.5,-4.45), init_x3(2.1,2.11), init_x4(1.5,1.51), init_u1, init_u2, intZero;
	std::vector<Interval> X0;
	X0.push_back(init_x1);
	X0.push_back(init_x2);
  X0.push_back(init_x3);
	X0.push_back(init_x4);
	X0.push_back(init_u1);
  X0.push_back(init_u2);

	pair<int, int> plot_dimensions = make_pair(0, 1);
	string filename_to_save = "./../../../Plots/Ex_10_.m";

	// translate the initial set to a flowpipe
	Flowpipe initial_set(X0, intZero);

	// the flowpipe that keeps the overapproximation at the end of a time horizon
	Flowpipe fp_last;

	// the symbolic remainder
	Symbolic_Remainder symb_rem(initial_set);

	std::list<Flowpipe> result;
	std::list<Flowpipe> flowpipes_end;

	flowpipes_end.push_back(initial_set);

	datatype max_difference = 0;

	std :: chrono :: steady_clock::time_point t1 = std :: chrono :: steady_clock::now();


	std:: chrono::duration< double > time_span;
	std :: chrono :: steady_clock::time_point start_time;
	std :: chrono :: steady_clock::time_point end_time;

	map< string, double > timing_information;

	compute_flowpipes_for_n_steps(
		X0, no_of_steps, no_of_flowpipes,
		polynomial_degree_for_controller, plant, crs,
		all_controllers, network_offsets, network_scaling_factors,
		result,  filename_to_save, plot_dimensions, timing_information
	);


	std :: chrono :: steady_clock :: time_point t2 = std::chrono::steady_clock::now();
	time_span = std :: chrono :: duration_cast < std :: chrono :: duration < double > > (t2-t1);
	cout << "Total execution time = " << time_span.count() << " second. " << endl;

	cout << "\% of time spent in regression = " << timing_information["time_in_regression"] * 100.0 << " \% " << endl;
	cout << "\% of  time spent in PWL construction = " << timing_information["time_in_pwl_construction"] * 100.0<< " \% "  << endl;
	cout << "\% of  time spent in Sherlock = " << timing_information["time_in_sherlock"] * 100.0 << " \% " <<  endl;
	cout << "\% of  time spent in Flowstar = " << timing_information["time_in_flowstar"] * 100.0 << " \% " << endl;
	cout << "Max Linear Pieces = " << timing_information["max_linear_pieces"] << endl;
	cout << "Max Error encountered = " << timing_information["max_error_encountered"] << endl;


	// plot the flowpipes in the x-y plane
	FILE *fp = fopen("./../../../Plots/Ex_10.m", "w");
	plot_2D_interval_MATLAB(fp, "x1", "x2", stateVars, result);
	fclose(fp);

	save_final_results_to_file(false, 10 ,30,0.01,2, timing_information["max_error_encountered"], time_span.count(),timing_information["time_in_regression"] * 100.0
	,timing_information["time_in_pwl_construction"] * 100.0, timing_information["time_in_sherlock"] * 100.0,
	timing_information["time_in_flowstar"] * 100.0, timing_information["max_linear_pieces"]);




  return 0;
}
