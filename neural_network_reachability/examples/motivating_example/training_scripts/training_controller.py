from __future__ import print_function
import tensorflow as tf
import numpy as np
# import matplotlib.pyplot as plt

no_of_state_vars = 2
no_of_control_in = 1

'''
data = np.empty([2,3])
data[0,0] = 1
data[0,1] = 2
data[0,2] = 3
data[1,0] = 4
data[1,1] = 5
data[1,2] = 6

tensor = tf.constant(data, shape=[2, 3])

#initialize the variable
init_op = tf.global_variables_initializer()

#run the graph
with tf.Session() as sess:
    sess.run(init_op) #execute init_op
    #print the random values that we sample
    print (sess.run(tensor))
'''

my_random_seed= 6476
np.random.seed(my_random_seed) # Make predictable
episodes = 200
batch_size = 5
# hidden_units = 500,100
hidden_units = 100
learning_rate = 0.0005
no_of_steps = 12
no_of_data_points = 225


def weight_variable(shape):
    initial = tf.truncated_normal(shape, stddev=0.1, seed=my_random_seed)
    return tf.Variable(initial)

def bias_variable(shape):
    initial = tf.constant(0.1, shape=shape)
    return tf.Variable(initial)

def relu(x):
    if x > 0:
        return x
    else:
        return 0


# Reading the weights and biases from the files.
data_stash = np.loadtxt("neural_network_information_1")

no_of_inputs = int(data_stash[0])
no_of_outputs = int(data_stash[1])
no_of_hidden_layers = int(data_stash[2])
if (no_of_hidden_layers!=1):
    print ("More than 1 hidden layer, needs adjustment")

no_of_neurons = int(data_stash[3])

weights_1_dim_1 = np.empty([no_of_inputs, no_of_neurons])
biases_1_dim_1 = np.empty([no_of_neurons])
pointer = 4
for i in range(no_of_neurons):
    row_weights = []
    for j in range(no_of_inputs):
        row_weights.append(data_stash[pointer])
        pointer = pointer + 1
    biases_1_dim_1[i] = (data_stash[pointer])
    pointer = pointer + 1
    weights_1_dim_1[:,i] = row_weights

weights_2_dim_1 = np.empty([no_of_neurons, no_of_outputs])
biases_2_dim_1 = np.empty([no_of_outputs])

for i in range(no_of_outputs):
    row_weights = []
    for j in range(no_of_neurons):
        row_weights.append(data_stash[pointer])
        pointer = pointer + 1
    biases_2_dim_1[i] = (data_stash[pointer])
    pointer = pointer + 1
    weights_2_dim_1[:,i] = row_weights

system_dim_1_w_1 = tf.constant(weights_1_dim_1, shape=[no_of_inputs, no_of_neurons], dtype = tf.float32)
system_dim_1_b_1 = tf.constant(biases_1_dim_1, shape=[no_of_neurons], dtype = tf.float32)
system_dim_1_w_2 = tf.constant(weights_2_dim_1, shape=[no_of_neurons, no_of_outputs], dtype = tf.float32)
system_dim_1_b_2 = tf.constant(biases_2_dim_1, shape=[no_of_outputs], dtype = tf.float32)

data_stash = np.loadtxt("neural_network_information_2")

no_of_inputs = int(data_stash[0])
no_of_outputs = int(data_stash[1])
no_of_hidden_layers = int(data_stash[2])
if (no_of_hidden_layers!=1):
    print ("More than 1 hidden layer, needs adjustment")

no_of_neurons = int(data_stash[3])

weights_1_dim_2 = np.empty([no_of_inputs, no_of_neurons])
biases_1_dim_2 = np.empty([no_of_neurons])
pointer = 4
for i in range(no_of_neurons):
    row_weights = []
    for j in range(no_of_inputs):
        row_weights.append(data_stash[pointer])
        pointer = pointer + 1
    biases_1_dim_2[i] = (data_stash[pointer])
    pointer = pointer + 1
    weights_1_dim_2[:,i] = row_weights

weights_2_dim_2 = np.empty([no_of_neurons, no_of_outputs])
biases_2_dim_2 = np.empty([no_of_outputs])

for i in range(no_of_outputs):
    row_weights = []
    for j in range(no_of_neurons):
        row_weights.append(data_stash[pointer])
        pointer = pointer + 1
    biases_2_dim_2[i] = (data_stash[pointer])
    pointer = pointer + 1
    weights_2_dim_2[:,i] = row_weights

system_dim_2_w_1 = tf.constant(weights_1_dim_2, shape=[no_of_inputs, no_of_neurons], dtype = tf.float32)
system_dim_2_b_1 = tf.constant(biases_1_dim_2, shape=[no_of_neurons], dtype = tf.float32)
system_dim_2_w_2 = tf.constant(weights_2_dim_2, shape=[no_of_neurons, no_of_outputs], dtype = tf.float32)
system_dim_2_b_2 = tf.constant(biases_2_dim_2, shape=[no_of_outputs], dtype = tf.float32)



# Create the input samples to be fit in
# create placeholders to pass the data to the model
x = tf.placeholder('float', shape=[None, no_of_inputs-no_of_control_in])
y_ = tf.placeholder('float', shape=[None, no_of_state_vars])


# Label the input as 'x', and use that to connect to the next layer

# Create the control network
W1 = weight_variable([no_of_inputs-no_of_control_in, hidden_units])
b1 = bias_variable([hidden_units])
r1 = tf.nn.relu( b1 + tf.matmul(x, W1) )

W2 = weight_variable([hidden_units, int(hidden_units/2)])
b2 = bias_variable([hidden_units/2])
r2 = tf.nn.relu( b2 + tf.matmul(r1, W2) )

W3 = weight_variable([int(hidden_units/2), 1])
b3 = bias_variable([1])
control_out = tf.nn.relu(tf.matmul(r2, W3) + b3)


# Create the threshold layers
max = 8.0
min = 0.0

# The minimum threshold
w_min_t_1 = np.empty([1,2])
b_min_t_1 = np.empty([2])

w_min_t_1[0,0] = 1.0
w_min_t_1[0,1] = -1.0

b_min_t_1[0] = 0.0
b_min_t_1[1] = min

w_min_t_2 = np.empty([2,1])
b_min_t_2 = np.empty([1])

w_min_t_2[0,0] = 1.0
w_min_t_2[1,0] = 1.0

b_min_t_2[0] = 0.0


# The maximum threshold
w_max_t_1 = np.empty([1,2])
b_max_t_1 = np.empty([2])

w_max_t_1[0,0] = 1.0
w_max_t_1[0,1] = 1.0

b_max_t_1[0] = 0.0
b_max_t_1[1] = -max

w_max_t_2 = np.empty([2,1])
b_max_t_2 = np.empty([1])

w_max_t_2[0,0] = 1.0
w_max_t_2[1,0] = -1.0

b_max_t_2[0] = 0.0



thres_w_1 = tf.constant(w_min_t_1, shape=[1, 2], dtype = tf.float32)
thres_b_1 = tf.constant(b_min_t_1, shape=[2], dtype = tf.float32)

thres_w_2 = tf.constant(w_min_t_2, shape=[2, 1], dtype = tf.float32)
thres_b_2 = tf.constant(b_min_t_2, shape=[1], dtype = tf.float32)

thres_w_3 = tf.constant(w_max_t_1, shape=[1, 2], dtype = tf.float32)
thres_b_3 = tf.constant(b_max_t_1, shape=[2], dtype = tf.float32)

thres_w_4 = tf.constant(w_max_t_2, shape=[2, 1], dtype = tf.float32)
thres_b_4 = tf.constant(b_max_t_2, shape=[1], dtype = tf.float32)


# scale the output of the control
b_shift = -4.0
w_scale = 1.0
weight_shift_and_scale_control = tf.constant(w_scale, shape=[1,1], dtype = tf.float32)
biases_shift_and_scale_control = tf.constant(b_shift, shape=[1,1], dtype = tf.float32)


# Add the weights and biases of the system model
# also make the proper connection for multiple dimensions

# shift and shrink the system output
scale_dim_1 = 10.0
shift_dim_1 = 100.0
scale_dim_2 = 10.0
shift_dim_2 = 100.0

weight_buffer_out = np.empty([2,2])
biases_buffer_out = np.empty([2])

weight_buffer_out[0,0] = 1.0/scale_dim_1
weight_buffer_out[1,0] = 0.0

weight_buffer_out[0,1] = 0.0
weight_buffer_out[1,1] = 1.0/scale_dim_2


biases_buffer_out[0] = -shift_dim_1/scale_dim_1
biases_buffer_out[1] = -shift_dim_2/scale_dim_2


weight_system_out = tf.constant(weight_buffer_out, shape=[2, 2], dtype = tf.float32)
biases_system_out = tf.constant(biases_buffer_out, shape=[2], dtype = tf.float32)

# Put everything together and assign the output to 'system_out'
out_1 = tf.nn.relu(tf.matmul(control_out, thres_w_1) + thres_b_1)
out_2 = tf.nn.relu(tf.matmul(out_1, thres_w_2) + thres_b_2)
out_3 = tf.nn.relu(tf.matmul(out_2, thres_w_3) + thres_b_3)
out_4 = tf.nn.relu(tf.matmul(out_3, thres_w_4) + thres_b_4)


out_5 = (tf.matmul( out_4, weight_shift_and_scale_control) + biases_shift_and_scale_control)

input_to_sys = tf.concat( [x, out_5] ,1)

dim_1_out_1 = tf.nn.relu(tf.matmul(input_to_sys, system_dim_1_w_1 ) + system_dim_1_b_1)
dim_1_out_2 = tf.nn.relu(tf.matmul(dim_1_out_1, system_dim_1_w_2) + system_dim_1_b_2)

dim_2_out_1 = tf.nn.relu(tf.matmul( input_to_sys, system_dim_2_w_1 ) + system_dim_2_b_1)
dim_2_out_2 = tf.nn.relu(tf.matmul(dim_2_out_1, system_dim_2_w_2) + system_dim_2_b_2)


raw_system_out = tf.concat( [dim_1_out_2, dim_2_out_2] ,1)

system_in_0 = (tf.matmul(raw_system_out, weight_system_out) + biases_system_out)


for step in range(no_of_steps):

    exec("r1_%d = tf.nn.relu(tf.matmul(system_in_%d, W1) + b1 )" %(step, step))
    exec("r2_%d = tf.nn.relu(tf.matmul(r1_%d, W2) + b2)" %(step, step) )
    exec("control_out_%d = tf.nn.relu(tf.matmul(r2_%d, W3) + b3)" %(step, step) )

    # Put everything together and assign the output to 'system_out'
    exec("out_1_%d = tf.nn.relu(tf.matmul(control_out_%d, thres_w_1) + thres_b_1)" %(step,step))
    exec("out_2_%d = tf.nn.relu(tf.matmul(out_1_%d, thres_w_2) + thres_b_2)" %(step, step) )
    exec("out_3_%d = tf.nn.relu(tf.matmul(out_2_%d, thres_w_3) + thres_b_3)" %(step, step) )
    exec("out_4_%d = tf.nn.relu(tf.matmul(out_3_%d, thres_w_4) + thres_b_4)" %(step, step) )


    exec("out_5_%d = (tf.matmul( out_4_%d, weight_shift_and_scale_control) + biases_shift_and_scale_control)" %(step, step) )

    exec("input_to_sys_%d = tf.concat( [system_in_%d, out_5_%d] ,1)" %(step, step, step) )

    exec("dim_1_out_1_%d = tf.nn.relu(tf.matmul(input_to_sys_%d, system_dim_1_w_1 ) + system_dim_1_b_1)" %(step, step) )
    exec("dim_1_out_2_%d = tf.nn.relu(tf.matmul(dim_1_out_1_%d, system_dim_1_w_2) + system_dim_1_b_2)" %(step, step) )

    exec("dim_2_out_1_%d = tf.nn.relu(tf.matmul( input_to_sys_%d, system_dim_2_w_1 ) + system_dim_2_b_1)" %(step, step))
    exec("dim_2_out_2_%d = tf.nn.relu(tf.matmul(dim_2_out_1_%d, system_dim_2_w_2) + system_dim_2_b_2)" %(step, step) )

    exec("raw_system_out_%d = tf.concat( [dim_1_out_2_%d, dim_2_out_2_%d] ,1)" %(step, step, step) )

    exec("system_out_%d = (tf.matmul(raw_system_out_%d, weight_system_out) + biases_system_out)" %(step, step) )

    exec("system_in_%d =  system_out_%d" %((step + 1), step))



x_data = np.zeros((no_of_data_points,no_of_inputs-no_of_control_in)).astype(np.float32)
y_data = np.zeros((no_of_data_points,no_of_inputs-no_of_control_in)).astype(np.float32)

rt_f = (no_of_data_points**(1/2.0) )
rt_i = int(rt_f)

for i in range(rt_i):
    for j in range(rt_i):
# Just generating a uniformly distributed data and normalizing it
        x_data[rt_i * i + j,0] = -1 + (2.0 * float(i))/rt_f
        x_data[rt_i * i + j,1] = -1 + (2.0 * float(j))/rt_f

np.random.shuffle(x_data)

for i in range(0,no_of_data_points):
    y_data[i, 0] = 0.0
    y_data[i, 1] = 0.0

exec("mean_square_error = tf.reduce_max(tf.square(system_in_%d-y_))" %(no_of_steps))
training = tf.train.AdamOptimizer(learning_rate).minimize(mean_square_error)

sess = tf.InteractiveSession()
sess.run(tf.global_variables_initializer())

min_error = np.inf
error_array_min=[]
error_array_all=[]
max_error = -np.inf
min_error = np.inf


for _ in range(episodes):
    # iterrate trough every row (with batch size)
    for i in range(x_data.shape[0]-batch_size+1):
        _, error = sess.run([training, mean_square_error],  feed_dict={x: x_data[i:i+batch_size,:], y_:y_data[i:i+batch_size,:]})
        if error > max_error:
            max_error = error
    print(max_error)
    if(max_error < min_error):
        min_error = max_error
        W_1 = sess.run(W1)
        b_1 = sess.run(b1)
        W_2 = sess.run(W2)
        b_2 = sess.run(b2)
        W_3 = sess.run(W3)
        b_3 = sess.run(b3)
        # W_4 = sess.run(W4)
        # b_4 = sess.run(b4)
        # W_5 = sess.run(W5)
        # b_5 = sess.run(b5)
        # W_6 = sess.run(W6)
        # b_6 = sess.run(b6)
        # W_7 = sess.run(W7)
        # b_7 = sess.run(b7)
        # W_8 = sess.run(W8)
        # b_8 = sess.run(b8)
        # W_14 = sess.run(W14)
        # b_14 = sess.run(b14)
        print('Min error recorded = ',min_error)

    max_error = -np.inf
        # print(error, x_data[i:i+batch_size], y_data[i:i+batch_size])


# Extracting the weights below :

filename = "neural_network_controller"
file = open(filename, "w")
# Write the number of inputs
s = str(no_of_inputs-no_of_control_in) + '\n'
file.writelines(s)
# Write the number of ouputs
s = '1' + '\n'
file.writelines(s)
# Write the number of layers (hidden)
s = '6' + '\n'
file.writelines(s)
# Write the number of neurons in each layer (hidden)
s = str(hidden_units) + '\n'
file.writelines(s)
s = str(hidden_units/2) + '\n'
file.writelines(s)
s = str(1) + '\n'
file.writelines(s)
s = str(2) + '\n'
file.writelines(s)
s = str(1) + '\n'
file.writelines(s)
s = str(2) + '\n'
file.writelines(s)
# # For each layer l write the value of weights crossed with the layer (l-1)
# # Do this for the layers from layer 1 to the output layer, thus for 'l' layers
# # we can have l+1 such sets
#
# # Writing weights in the connection of input layer with the 1st hidden layer
#
weights = W_1
biases = b_1

for i in range(hidden_units):
    for j in range(no_of_inputs-no_of_control_in):
        x = weights[j,i]
        s = str(x) + '\n'
        file.writelines(s)
    x = biases[i]
    s = str(x)+ '\n'
    file.writelines(s)

# # Writing weights in the connection of 1st layer hidden with the 2nd hidden layer
#
weights = W_2
biases = b_2
#
for i in range(int(hidden_units/2)):
    for j in range(hidden_units):
        x = weights[j,i]
        s = str(x) + '\n'
        file.writelines(s)
    x = biases[i]
    s = str(x) + '\n'
    file.writelines(s)

# # # Writing weights in the connection of 2nd hidden layer with the 3rd hidden layer
# #
# weights = W_3
# biases = b_3
# #
# # W_3 = weights
# # b_3 = biases
# #
# for i in range(hidden_units):
#     for j in range(hidden_units):
#         x = weights[j,i]
#         s = str(x) + '\n'
#         file.writelines(s)
#     x = biases[i]
#     s = str(x) + '\n'
#     file.writelines(s)
# #
# # # # Writing weights in the connection of 2nd hidden layer with the 3rd hidden layer
# # #
# weights = W_4
# biases = b_4
# #
# # W_3 = weights
# # b_3 = biases
# #
# for i in range(hidden_units):
#     for j in range(hidden_units):
#         x = weights[j,i]
#         s = str(x) + '\n'
#         file.writelines(s)
#     x = biases[i]
#     s = str(x) + '\n'
#     file.writelines(s)
# # # Writing weights in the connection of 2nd hidden layer with the 3rd hidden layer
# #
# weights = W_5
# biases = b_5
# #
# # W_3 = weights
# # b_3 = biases
# #
# for i in range(hidden_units):
#     for j in range(hidden_units):
#         x = weights[j,i]
#         s = str(x) + '\n'
#         file.writelines(s)
#     x = biases[i]
#     s = str(x) + '\n'
#     file.writelines(s)
# # # Writing weights in the connection of 2nd hidden layer with the 3rd hidden layer
# #
# weights = W_6
# biases = b_6
# #
# # W_3 = weights
# # b_3 = biases
# #
# for i in range(hidden_units):
#     for j in range(hidden_units):
#         x = weights[j,i]
#         s = str(x) + '\n'
#         file.writelines(s)
#     x = biases[i]
#     s = str(x) + '\n'
#     file.writelines(s)
# # # Writing weights in the connection of 2nd hidden layer with the 3rd hidden layer
# #
# weights = W_7
# biases = b_7
# #
# # W_3 = weights
# # b_3 = biases
# #
# for i in range(hidden_units):
#     for j in range(hidden_units):
#         x = weights[j,i]
#         s = str(x) + '\n'
#         file.writelines(s)
#     x = biases[i]
#     s = str(x) + '\n'
#     file.writelines(s)

# # Writing weights in the connection of 3rd hidden layer with the output layer
#
weights = W_3
biases = b_3

# W_4 = weights
# b_4 = biases
#
for i in range(int(hidden_units/2)):
    x = weights[i,0]
    s = str(x) + '\n'
    file.writelines(s)

x = biases[0]
s = str(x) + '\n'
file.writelines(s)

# Writing the threshold layers

x = w_min_t_1[0,0]
s = str(x) + '\n'
file.writelines(s)

x = b_min_t_1[0]
s = str(x) + '\n'
file.writelines(s)

x = w_min_t_1[0,1]
s = str(x) + '\n'
file.writelines(s)

x = b_min_t_1[1]
s = str(x) + '\n'
file.writelines(s)
#######################

x = w_min_t_2[0,0]
s = str(x) + '\n'
file.writelines(s)

x = w_min_t_2[1,0]
s = str(x) + '\n'
file.writelines(s)

x = b_min_t_2[0]
s = str(x) + '\n'
file.writelines(s)
#####################

x = w_max_t_1[0,0]
s = str(x) + '\n'
file.writelines(s)

x = b_max_t_1[0]
s = str(x) + '\n'
file.writelines(s)

x = w_max_t_1[0,1]
s = str(x) + '\n'
file.writelines(s)

x = b_max_t_1[1]
s = str(x) + '\n'
file.writelines(s)
#######################

x = w_max_t_2[0,0]
s = str(x) + '\n'
file.writelines(s)

x = w_max_t_2[1,0]
s = str(x) + '\n'
file.writelines(s)

x = b_max_t_2[0]
s = str(x) + '\n'
file.writelines(s)
#####################


file.close()

sess.close()


# print("should be: min_error: 7.44752e-05")
