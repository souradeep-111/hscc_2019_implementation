#include "./headers/propagate_intervals.h"
#include "../flowstar-release/Continuous.h"

using namespace std;
using namespace flowstar;

datatype offset_in_constraint_comb = constr_comb_offset;
// So the above data assumes, that this number is
// same for all the networks you have in the setting,
// and also all the networks are single output network
// with the first one giving the output

void construct_garbage_layer_but_copy_first(vector< vector< datatype > >& weights,
	vector< datatype >& biases, int no_of_outputs, int no_of_inputs);

int main()
{
  // Simple range propagation
	char controller_file[] = "quadrotor_controller" ;
	network_handler system_network(controller_file);


  // char saving_name[] = "modified_quadrotor_controller";
	//
  // vector< vector< vector< datatype > > > weights;
  // vector< vector< datatype > > biases;
	//
  // system_network.return_network_information(weights, biases);
	//
  // unsigned int no_of_inner_neurons = 100;
	//
  unsigned int i, j ,k;
	//
  // vector< vector< vector < datatype > > > new_weights;
  // vector< vector < datatype > > new_biases;
  // vector< vector< datatype > > weight_matrix;
  // vector< datatype > buffer_biases;
  // vector< datatype > weight_vector;
	//
  // unsigned int no_of_layers_to_be_added = 1;
	//
  // // i = 0;
  // // while(i < no_of_layers_to_be_added)
  // // {
  // //   weight_matrix.clear();
  // //   buffer_biases.clear();
  // //   if(!i)
  // //   {
  // //     weight_matrix = weights[0];
  // //     buffer_biases.push_back(biases[0][0]);
	// //
  // //     j = 0;
  // //     while(j < no_of_inner_neurons-1)
  // //     {
  // //       weight_vector.resize(system_network.no_of_inputs);
  // //       fill(weight_vector.begin(), weight_vector.end(), 0.0);
	// //
  // //       weight_matrix.push_back(weight_vector);
  // //       buffer_biases.push_back(0.0);
  // //       j++;
  // //     }
  // //     new_weights.push_back(weight_matrix);
  // //     new_biases.push_back(buffer_biases);
  // //   }
  // //   else
  // //   {
  // //     add_fake_layer_to_right(new_weights, new_biases);
  // //   }
  // //   i++;
  // // }
	//
	// i = 0;
	// while(i < no_of_layers_to_be_added)
	// {
	//   weight_matrix.clear();
	//   buffer_biases.clear();
	//   if(!i)
	//   {
	//     weight_matrix = weights[0];
	//     buffer_biases.push_back(biases[0][0]);
	//
	//     j = 0;
	//     while(j < no_of_inner_neurons-1)
	//     {
	//       weight_vector.resize(system_network.no_of_inputs);
	//       fill(weight_vector.begin(), weight_vector.end(), 0.0);
	//
	//       weight_matrix.push_back(weight_vector);
	//       buffer_biases.push_back(0.0);
	//       j++;
	//     }
	//     new_weights.push_back(weight_matrix);
	//     new_biases.push_back(buffer_biases);
	//   }
	//   else
	//   {
	//     add_fake_layer_to_right(new_weights, new_biases);
	//   }
	//   i++;
	// }
	//
  // weight_matrix.clear();
  // buffer_biases.clear();
	//
  // weight_vector.clear();
  // weight_vector.resize(no_of_inner_neurons);
  // fill(weight_vector.begin(), weight_vector.end(), 0.0);
  // weight_vector[0] = 1;
	//
  // weight_matrix.push_back(weight_vector);
  // buffer_biases.push_back(0);
	//
	//
  // new_weights.push_back(weight_matrix);
  // new_biases.push_back(buffer_biases);
	//
	//
  // write_network_to_file(new_weights, new_biases, saving_name);
  // network_handler modified_network(saving_name);
	//
	//

  unsigned int no_of_inputs = system_network.no_of_inputs;

  vector< vector< datatype > > input_interval(no_of_inputs, vector< datatype >(2,0) );
  i = 0;
  while(i < no_of_inputs)
  {
    input_interval[i][0] = 0;
    input_interval[i][1] = 2;
    i++;
  }
  vector< vector< datatype > > input_constraints;
  create_constraint_from_interval(input_constraints, input_interval);

  vector< datatype > output_range(2);
  system_network.return_interval_output(input_constraints, output_range, 1);

  cout << "Output range = " << output_range[0] << "  " << output_range[1] << endl;

  return 0;
}

void construct_garbage_layer_but_copy_first(vector< vector< datatype > >& weights,
	vector< datatype >& biases, int no_of_inputs, int no_of_outputs)
{
		weights.clear();
		biases.clear();

		unsigned int i, j , k;

		vector< datatype > weight_vector(no_of_inputs);
		fill(weight_vector.begin(), weight_vector.end(), 0.0);
		weight_vector[0] = 1;

		biases.push_back(0.0);


		i = 0;
		while(i < no_of_outputs)
		{
			i++;
		}


}
