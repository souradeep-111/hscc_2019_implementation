#!/bin/bash
echo "--- Compiling all the experiments --- "
echo " ====================================================================== "
rm -r Plots
mkdir Plots
cd ./neural_network_reachability/examples/tests/
cd Results
rm results_table
cd ..
# make clean
make

echo " --- Compilation done --- "
echo " ======================================================================="
echo " --- Running Example 1 , printing the reach sets --- "
./Ex_1
cd ../../../Plots/
echo "title(\"Flowpipe for Ex. 1\"); print -deps plot_1.eps; " >> Ex_1.m
octave Ex_1.m
cd ../neural_network_reachability/examples/tests/
echo " --- Results have been saved into ~/Documents/Plots/plot_1.eps -- "

echo " --- Running Example 2 , printing the reach sets --- "
./Ex_2
cd ../../../Plots/
echo "title(\"Flowpipe for Ex. 2\"); print -deps plot_2.eps; " >> Ex_2.m
octave Ex_2.m
cd ../neural_network_reachability/examples/tests/
echo " --- Results have been saved into ~/Documents/Plots/plot_2.eps -- "

echo " --- Running Example 3 , printing the reach sets --- "
./Ex_3
cd ../../../Plots/
echo "title(\"Flowpipe for Ex. 3\"); print -deps plot_3.eps; " >> Ex_3.m
octave Ex_3.m
cd ../neural_network_reachability/examples/tests/
echo " --- Results have been saved into ~/Documents/Plots/plot_3.eps -- "

echo " --- Running Example 4 , printing the reach sets --- "
./Ex_4
cd ../../../Plots/
echo "title(\"Flowpipe for Ex. 4\"); print -deps plot_4.eps; " >> Ex_4.m
octave Ex_4.m
cd ../neural_network_reachability/examples/tests/
echo " --- Results have been saved into ~/Documents/Plots/plot_4.eps -- "

echo " --- Running Example 5 , printing the reach sets --- "
./Ex_5
cd ../../../Plots/
echo "title(\"Flowpipe for Ex. 5\"); print -deps plot_5.eps; " >> Ex_5.m
octave Ex_5.m
cd ../neural_network_reachability/examples/tests/
echo " --- Results have been saved into ~/Documents/Plots/plot_5.eps -- "

echo " --- Running Example 6 , printing the reach sets --- "
./Ex_6
cd ../../../Plots/
echo "title(\"Flowpipe for Ex. 6\"); print -deps plot_6.eps; " >> Ex_6.m
octave Ex_6.m
cd ../neural_network_reachability/examples/tests/
echo " --- Results have been saved into ~/Documents/Plots/plot_6.eps -- "

echo " --- Running Example 7 , printing the reach sets --- "
./Ex_7
cd ../../../Plots/
echo "title(\"Flowpipe for Ex. 7\"); print -deps plot_7.eps; " >> Ex_7.m
octave Ex_7.m
cd ../neural_network_reachability/examples/tests/
echo " --- Results have been saved into ~/Documents/Plots/plot_7.eps -- "

echo " --- Running Example 8 , printing the reach sets --- "
./Ex_8
cd ../../../Plots/
echo "title(\"Flowpipe for Ex. 8\"); print -deps plot_8.eps; " >> Ex_8.m
octave Ex_8.m
cd ../neural_network_reachability/examples/tests/
echo " --- Results have been saved into ~/Documents/Plots/plot_8.eps -- "

echo " --- Running Example 9 , printing the reach sets --- "
./Ex_9
cd ../../../Plots/
echo "title(\"Flowpipe for Ex. 9\"); print -deps plot_9.eps; " >> Ex_9.m
octave Ex_9.m
cd ../neural_network_reachability/examples/tests/
echo " --- Results have been saved into ~/Documents/Plots/plot_9.eps -- "

echo " --- Running Example 10 , printing the reach sets --- "
./Ex_10
cd ../../../Plots/
echo "title(\"Flowpipe for Ex. 10\"); print -deps plot_10.eps; " >> Ex_10.m
octave Ex_10.m
cd ../neural_network_reachability/examples/tests/
echo " --- Results have been saved into ~/Documents/Plots/plot_10.eps -- "

echo " --- Running Quadrotor Example , printing the reach sets --- "
./Ex_Quadrotor
cd ../../../Plots/
echo "title(\"Flowpipe for Ex. Quadrotor\"); print -deps plot_Quadrotor.eps; " >> Ex_Quadrotor.m
octave Ex_Quadrotor.m
cd ../neural_network_reachability/examples/tests/
echo " --- Results have been saved into ~/Documents/Plots/plot_Quadrotor.eps -- "

# ./Ex_12_PR -- 1
# ./Ex_13_PR -- 2
# ./Ex_14_PR -- 3
# ./Ex_15_PR -- 4
# ./Ex_16_PR -- 5
# ./Ex_17_PR -- 6
# ./Ex_18_PR -- 7
# ./Ex_19_PR -- 8
# ./Ex_Tora_PR -- 9
# ./Ex_Car_Model_PR -- 10
# ./Ex_Quadrotor_PR
cd Results
./pandoc results_table -V geometry:margin=1in -V fontsize:1pt -s -o results_table.pdf
cp results_table.pdf ../../../../

echo "The table has been saved into ~/Documents/results_table.pdf"
